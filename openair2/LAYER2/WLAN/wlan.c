/*
  AUTHOR  : Xuhang Ying
  DATE	  : Aug 2015
*/

#include "wlan.h"
#include "mem_block.h"
#include "../MAC/extern.h"
#include "UTIL/LOG/log.h"
#include "UTIL/OCG/OCG_vars.h"
#include "UTIL/LOG/vcd_signal_dumper.h"

#include "assertions.h"

extern boolean_t lwa_data_ind(
  const protocol_ctxt_t* const ctxt_pP,
  const srb_flag_t srb_flagP,
  const MBMS_flag_t MBMS_flagP,
  const rb_id_t rb_idP,
  const sdu_size_t sdu_buffer_sizeP,
  mem_block_t* sdu_buffer_pP,
  const boolean_t lte_flagP);

extern void lwa_util_print_hex_octets(
  comp_name_t componentP, 
  unsigned char* dataP, 
  const signed long sizeP);

// Global variable that stores WLAN packets; used for emulation
wlan_buffer_t *wlan_buffer_dl = NULL;
wlan_buffer_t *wlan_buffer_ul = NULL; 

hash_table_t *wlan_coll_p;

//-----------------------------------------------------------------------------
wlan_op_status_t 
wlan_data_req (
            const protocol_ctxt_t* const ctxt_pP,
            const srb_flag_t   srb_flagP,
            const MBMS_flag_t  MBMS_flagP,
            const rb_id_t      rb_idP,
            const mui_t        muiP,
            confirm_t    confirmP,
            sdu_size_t   sdu_sizeP,
            mem_block_t *sdu_pP)            // PDCP PDU
{
  /* Input:  WLAN SDU (PDCP PDU)
     Output: WLAN PDU (WLAN pkt)
  */
  uint32_t        wlan_header_size  = 0;
  uint32_t        wlan_tailer_size  = 0;
  uint32_t        wlan_payload_size = 0;
  uint32_t        wlan_pkt_size     = 0;

  mem_block_t     *wlan_sdu_p       = NULL;     // PDCP PDU (WLAN SDU)
  wlan_pkt_t      *wlan_pkt_p       = NULL;
  wlan_buffer_t   *wlan_buffer_p    = NULL;

  /* Create a new memory block for PDCP PDU */
  //wlan_sdu_p = get_free_mem_block(sdu_sizeP);
  //memcpy(&wlan_sdu_p->data[0], &sdu_pP->data[0], sdu_sizeP);
  //free_mem_block(sdu_pP);
  wlan_sdu_p = sdu_pP;

  /* Prepare a WLAN pkt */
  wlan_payload_size = sdu_sizeP;
  wlan_pkt_size = wlan_payload_size + wlan_header_size + wlan_tailer_size;

  wlan_pkt_p = calloc(1, sizeof(wlan_pkt_t));
  wlan_pkt_p->payload   = wlan_sdu_p;
  wlan_pkt_p->srb_flag  = srb_flagP;
  wlan_pkt_p->MBMS_flag = MBMS_flagP;
  wlan_pkt_p->rb_id     = rb_idP;

  wlan_pkt_p->payload_size  = wlan_payload_size;
  wlan_pkt_p->packet_size   = wlan_pkt_size;

  /* Send the WLAN pkt */
  /* Naive implemenetation for single UE
        - Put outgoing WLAN packets (PDCP PDU) in a buffer
        - Assumes a single UE (otherwise, identifiers are needed)
        - Later, this should be implemented with a hashtable accessed by key derived from context
  */
  wlan_buffer_p = (ctxt_pP->enb_flag)? wlan_buffer_dl:wlan_buffer_ul;

  pthread_mutex_lock(&wlan_buffer_p->lock_buffer);
  wlan_pkt_list_add_tail(wlan_pkt_p, &wlan_buffer_p->pkts_list);
  pthread_mutex_unlock(&wlan_buffer_p->lock_buffer);

  LOG_I(WLAN, "[SHAUN][wlan_data_req][%s] Mod_id %d Frame %d Put a PDCP PDU (%p) data (%p) of size %d\n",
        ctxt_pP->enb_flag? "eNB":" UE",
        ctxt_pP->module_id,
        ctxt_pP->frame,
        wlan_pkt_p->payload,
        (wlan_pkt_p->payload)->data,
        wlan_pkt_p->payload_size);

  //lwa_util_print_hex_octets(WLAN, (unsigned char*)(wlan_pkt_p->payload)->data, 16);

  // LOG_I(WLAN, "[SHAUN] %s: HEAD = %p, TAIL = %p, nb_elements = %d\n",
  //       (ctxt_pP->enb_flag)? "DL":"UL",
  //       wlan_pkt_list_get_head(&wlan_buffer_p->pkts_list),
  //       wlan_pkt_list_get_tail(&wlan_buffer_p->pkts_list),
  //       (wlan_buffer_p->pkts_list).nb_elements);

  return WLAN_OP_STATUS_OK;
  //return rlc_data_req(ctxt_pP, srb_flagP, MBMS_flagP, rb_idP, muiP, confirmP, sdu_sizeP, sdu_pP);
}


//-----------------------------------------------------------------------------
void 
wlan_data_ind (const protocol_ctxt_t* const ctxt_pP)
{
  srb_flag_t      srb_flag;
  MBMS_flag_t     MBMS_flag;
  rb_id_t         rb_id;

  wlan_pkt_t      *wlan_pkt_p   = NULL;
  wlan_buffer_t   *wlan_buffer_p = NULL; 

  wlan_buffer_p = (ctxt_pP->enb_flag)? wlan_buffer_ul:wlan_buffer_dl;

  while((wlan_buffer_p->pkts_list).nb_elements > 0){
    /* Receive WLAN pkts */
    pthread_mutex_lock(&wlan_buffer_p->lock_buffer);
    wlan_pkt_p = wlan_pkt_list_remove_head(&wlan_buffer_p->pkts_list);
    pthread_mutex_unlock(&wlan_buffer_p->lock_buffer);

    if (wlan_pkt_p != NULL){
      srb_flag      = wlan_pkt_p->srb_flag;
      MBMS_flag     = wlan_pkt_p->MBMS_flag;
      rb_id         = wlan_pkt_p->rb_id;

      LOG_I(WLAN, "[SHAUN][wlan_data_ind][%s] Mod_id %d Frame %d Got a PDCP PDU (%p) data (%p) of size %d\n",
            (ctxt_pP->enb_flag)?"eNB":" UE",
            ctxt_pP->module_id,
            ctxt_pP->frame,
            wlan_pkt_p->payload,
            (wlan_pkt_p->payload)->data,
            wlan_pkt_p->payload_size);

      //lwa_util_print_hex_octets(WLAN, (unsigned char*)(wlan_pkt_p->payload)->data, 16);

      // LOG_I(WLAN, "[SHAUN] %s: HEAD = %p, TAIL = %p, nb_elements = %d\n",
      //       (ctxt_pP->enb_flag)? "UL":"DL",
      //       wlan_pkt_list_get_head(&wlan_buffer_p->pkts_list),
      //       wlan_pkt_list_get_tail(&wlan_buffer_p->pkts_list),
      //       (wlan_buffer_p->pkts_list).nb_elements);

      // LOG_I(WLAN, "%s, srb_flag  = %d, MBMS_flag  = %d, rb_id  = %d\n", 
      //       (ctxt_pP->enb_flag)? "eNB":" UE", 
      //       srb_flag, 
      //       MBMS_flag, 
      //       rb_id);

      /* Pass it to LWA */
      lwa_data_ind(
        ctxt_pP,
        srb_flag,
        MBMS_flag,
        rb_id,
        wlan_pkt_p->payload_size,
        wlan_pkt_p->payload,
        0);           // Shaun: via WLAN

      free(wlan_pkt_p);

    }else{
      LOG_E(WLAN, "[SHAUN] received WLAN pkt is NULL\n");
      break;
    }
  }
}

void 
wlan_module_free(void *wlan_pP){
  ;
}


int 
wlan_module_init(void){
  LOG_D(WLAN, "WLAN MODULE INIT\n");

  //wlan_coll_p = hashtable_create((maxDRB + 2) * 16, NULL, wlan_module_free);
  //AssertFatal(wlan_coll_p != NULL, "UNRECOVERABLE error, WLAN hashtable_create failed");

  wlan_buffer_init();
  return(0);
}

void 
wlan_buffer_init(void){
  // Shaun: eNB <--> single UE
  if (wlan_buffer_dl == NULL){
    wlan_buffer_dl = calloc(1, sizeof(wlan_buffer_t));
    list_init(&wlan_buffer_dl->pkts_list, "WLAN DL Buffer");

    LOG_I(WLAN, "[SHAUN][wlan_buffer_init] wlan_buffer_dl: HEAD = %p, TAIL = %p, nb_elements = %d\n",
          list_get_head(&wlan_buffer_dl->pkts_list),
          list_get_tail(&wlan_buffer_dl->pkts_list),
          (wlan_buffer_dl->pkts_list).nb_elements);
    
    wlan_buffer_dl->buffer_occupancy = 0;
    wlan_buffer_dl->dl_flag = 1;        // eNB --> buffer --> UE
    wlan_buffer_dl->module_id = 0;      // Module id for eNB
    LOG_D(WLAN, "[SHAUN] emulation WLAN DL buffer is created for eNB\n");
    
  }else{
    LOG_E(WLAN, "[SHAUN] emulation WLAN DL buffer has already been initialized. \n");
  }

  if (wlan_buffer_ul == NULL){
    wlan_buffer_ul = calloc(1, sizeof(wlan_buffer_t));
    list_init(&wlan_buffer_ul->pkts_list, "WLAN UL Buffer");

    LOG_I(WLAN, "[SHAUN][wlan_buffer_init] wlan_buffer_ul: HEAD = %p, TAIL = %p, nb_elements = %d\n",
          list_get_head(&wlan_buffer_ul->pkts_list),
          list_get_tail(&wlan_buffer_ul->pkts_list),
          (wlan_buffer_ul->pkts_list).nb_elements);

    wlan_buffer_ul->buffer_occupancy = 0;
    wlan_buffer_ul->dl_flag = 0;        // UE --> buffer --> eNB
    wlan_buffer_ul->module_id = 0;      // Module id for UE

    LOG_D(WLAN, "[SHAUN] emulation WLAN UL buffer is created for ONE UE\n");
  }else{
    LOG_E(WLAN, "[SHAUN] emulation WLAN UL buffer has already been initialized. \n");
  }
}

void 
wlan_buffer_free(const wlan_buffer_t* wlan_buffer_pP){
  if (&wlan_buffer_pP->pkts_list != NULL){
    list_free( (const struct list_t *) &wlan_buffer_pP->pkts_list);
  }

  if (wlan_buffer_pP != NULL){
    free(wlan_buffer_pP);
    wlan_buffer_pP = NULL;
  }
}


void 
wlan_pkt_list_init(wlan_pkt_list_t * listP, char * nameP)
{
  if (nameP){
    strncpy(listP->name, nameP, LIST_NAME_MAX_CHAR);
    listP->name[LIST_NAME_MAX_CHAR-1] = 0;
  }

  listP->tail = NULL;
  listP->head = NULL;
  listP->nb_elements = 0;

}

void 
wlan_pkt_list_free(wlan_pkt_list_t * listP)
{
  wlan_pkt_t    *wlan_pkt_p;

  while((wlan_pkt_p = wlan_pkt_list_remove_head(listP))){
    free_mem_block(wlan_pkt_p->payload);
  }
}

wlan_pkt_t * 
wlan_pkt_list_get_head(wlan_pkt_list_t * listP)
{
  return listP->head;
}

wlan_pkt_t * 
wlan_pkt_list_get_tail(wlan_pkt_list_t * listP)
{
  return listP->tail;
}

wlan_pkt_t * 
wlan_pkt_list_remove_head(wlan_pkt_list_t * listP)
{
  wlan_pkt_t    *head;
  head = listP->head;

  if (head != NULL) {
    head->next = NULL;    // the removed element does not link to any element

    listP->head = head->next;
    listP->nb_elements = listP->nb_elements - 1;

    // If there was only one element, update the tail of the list after the removal
    if (listP->head == NULL){
      listP->tail = NULL;
    }
  } else {
    //msg("[MEM_MGT][WARNING] remove_head_from_list(%s) no elements\n",listP->name);
  }

  return head;
}

void 
wlan_pkt_list_add_tail(wlan_pkt_t * elementP, wlan_pkt_list_t * listP)
{
  wlan_pkt_t       *tail;
  wlan_pkt_t       *head;

  if (elementP != NULL){
    tail = listP->tail;   // current tail

    if (tail == NULL){
    // no element in the current list  
      listP->nb_elements = listP->nb_elements + 1;
      elementP->next = NULL;
      //elementP->previous = NULL;

      listP->head = elementP;
      listP->tail = elementP;
    }else{ 
    // there is more than one element in the current list
      listP->nb_elements = listP->nb_elements + 1;
      elementP->next = NULL;
      //elementP->previous = tail;  // link the new element with the current tail
      tail->next = elementP;      // link the current tail with the new element

      listP->tail = elementP;     // update the tail of the list
    }
  }
}