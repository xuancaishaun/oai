/*! \file wlan.h
* \brief This file, and only this file must be included by external code that interact with WLAN.
* \author Xuhang Ying
* \date Aug 2015
* \version 0
* \note
* \bug
* \warning
*/

#include "platform_types.h"
#include "platform_constants.h"
#include "hashtable.h"
#include "UTIL/LOG/log.h"
#include "mem_block.h"
#include "rlc.h"

#define WLAN_OP_STATUS_OK                 1
#define WLAN_OP_STATUS_INTERNAL_ERROR     2

typedef int32_t             wlan_buffer_occupancy_t;
typedef signed int          wlan_op_status_t;

//----------------------------------------------------
typedef struct wlan_entity_s{
  pthread_mutex_t           lock_wlan_pkts;
  list_t                    wlan_pkts;
  wlan_buffer_occupancy_t   buffer_occupancy;

} wlan_entity_t;

//----------------------------------------------------
typedef struct wlan_pkt_s
{
  /* WLAN header */
  srb_flag_t    srb_flag;
  MBMS_flag_t   MBMS_flag;
  rb_id_t       rb_id;

  uint32_t      payload_size;   // PDCP PDU size
  uint32_t      packet_size;    // PDCP PDU size + WLAN header & tailer size

  /* WLAN payload */
  mem_block_t   *payload;       // PDCP PDU

  struct wlan_pkt_t *next;
  struct wlan_pkt_t *previous;
} wlan_pkt_t;

//----------------------------------------------------
typedef struct wlan_pkt_list_s
{ 
  struct wlan_pkt_t   *head;
  struct wlan_pkt_t   *tail;
  int                 nb_elements;
  char                name[LIST_NAME_MAX_CHAR];
} wlan_pkt_list_t;
//----------------------------------------------------

typedef struct wlan_buffer_s
{
  // Buffer parameters
  pthread_mutex_t         lock_buffer;
  wlan_pkt_list_t         pkts_list;
  wlan_buffer_occupancy_t buffer_occupancy;

  // eNB/UE info
  boolean_t               dl_flag;      /*!< \brief Is this buffer for downlink (1) or uplink (0) */
  module_id_t             module_id;    /*!< \brief Virtualized module identifier (SHAUN: UE instance identifier?)*/
} wlan_buffer_t;
//----------------------------------------------------

extern hash_table_t *wlan_coll_p;
extern wlan_buffer_t *wlan_buffer_dl;
extern wlan_buffer_t *wlan_buffer_ul;

wlan_op_status_t wlan_data_req(
            const protocol_ctxt_t* const,
            const srb_flag_t,
            const MBMS_flag_t,
            const rb_id_t,
            const mui_t,
            const confirm_t,
            const sdu_size_t,
            mem_block_t * const);

void wlan_data_ind(const protocol_ctxt_t* const);

int wlan_module_init(void);

void wlan_module_free(void *wlan_pP);

void wlan_buffer_init(void);

void wlan_buffer_free(const wlan_buffer_t*);

void wlan_pkt_list_init(wlan_pkt_list_t*, char*);
void wlan_pkt_list_free(wlan_pkt_list_t*);
wlan_pkt_t * wlan_pkt_list_get_head(wlan_pkt_list_t*);
wlan_pkt_t * wlan_pkt_list_get_tail(wlan_pkt_list_t*);
wlan_pkt_t * wlan_pkt_list_remove_head(wlan_pkt_list_t*);
void wlan_pkt_list_add_tail(wlan_pkt_t*, wlan_pkt_list_t*);