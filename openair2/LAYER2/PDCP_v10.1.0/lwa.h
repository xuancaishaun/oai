//-----------------------------------------------------------------------------
#ifndef NON_ACCESS_STRATUM
#include "UTIL/MEM/mem_block.h"
#include "UTIL/LISTS/list.h"
#include "COMMON/mac_rrc_primitives.h"
#endif //NON_ACCESS_STRATUM
//-----------------------------------------------------------------------------

#include "COMMON/platform_constants.h"
#include "COMMON/platform_types.h"
#include "DRB-ToAddMod.h"
#include "DRB-ToAddModList.h"
#include "SRB-ToAddMod.h"
#include "SRB-ToAddModList.h"
#include "pdcp.h"
#ifdef Rel10
#include "MBMS-SessionInfoList-r9.h"
#include "PMCH-InfoList-r9.h"
#endif

#define LWA_RLC       0
#define LWA_WLAN      1

typedef int32_t             lwa_buffer_occupancy_t;

typedef struct lwa_entity_s {
  boolean_t   is_defined;
  srb_flag_t  srb_flag;

  /* SN */
  pdcp_sn_t   current_seq_num;
  pdcp_sn_t   previous_rx_seq_num; 
  pdcp_sn_t   expected_rx_seq_num;

  /* Buffer for incoming packets from RLC and WLAN */
  pthread_mutex_t         lock_lwa_buffer;
  list_t                  lwa_buffer;
  lwa_buffer_occupancy_t  buffer_occupancy;
} lwa_entity_t;

boolean_t lwa_data_req(
      const protocol_ctxt_t* const  ctxt_pP,
      const srb_flag_t srb_flagP,
      const MBMS_flag_t ,
      const rb_id_t rb_id,
      const mui_t muiP,
      const confirm_t confirmP,
      const sdu_size_t sdu_buffer_size,
      mem_block_t * const pdcp_pdu_p,
      pdcp_t * const pdcp_p);

boolean_t lwa_data_ind(
      const protocol_ctxt_t* const ctxt_pP,
      const srb_flag_t srb_flagP,
      const MBMS_flag_t MBMS_flagP,
      const rb_id_t rb_idP,
      const sdu_size_t sdu_buffer_sizeP,
      mem_block_t* const sdu_buffer_pP,
      const boolean_t);    // 1: from RLC; 0: from WLAN

boolean_t lwa_traffic_management(lwa_entity_t * const lwa_p);
void lwa_sublayer_init(void);
void lwa_entity_init(lwa_entity_t * const lwa_pP);
void lwa_entity_free(lwa_entity_t * const lwa_pP);

void lwa_util_print_hex_octets(comp_name_t componentP, unsigned char* dataP, const signed long sizeP);