#include "lwa.h"
#include "assertions.h"
#include "hashtable.h"
#include "pdcp.h"
#include "pdcp_util.h"
#include "pdcp_sequence_manager.h"
#include "LAYER2/RLC/rlc.h"
#include "LAYER2/WLAN/wlan.h"
#include "LAYER2/MAC/extern.h"
#include "RRC/L2_INTERFACE/openair_rrc_L2_interface.h"
#include "pdcp_primitives.h"
#include "OCG.h"
#include "OCG_extern.h"
#include "otg_rx.h"
#include "UTIL/LOG/log.h"
#include <inttypes.h>
#include "platform_constants.h"
#include "UTIL/LOG/vcd_signal_dumper.h"
#include "msc.h"
#include <stdio.h>


#if defined(ENABLE_SECURITY)
# include "UTIL/OSA/osa_defs.h"
#endif

#if defined(ENABLE_ITTI)
# include "intertask_interface.h"
#endif

#if defined(LINK_ENB_PDCP_TO_GTPV1U)
#  include "gtpv1u_eNB_task.h"
#  include "gtpv1u.h"
#endif

#ifndef OAI_EMU
extern int otg_enabled;
#endif


boolean_t 
lwa_data_req( const protocol_ctxt_t* const ctxt_pP,
              const srb_flag_t  srb_flagP,
              const MBMS_flag_t MBMS_flagP,
              const rb_id_t     rb_idP,
              const mui_t       muiP,
              const confirm_t   confirmP,
              const sdu_size_t  pdcp_pdu_sizeP,
              mem_block_t       *pdcp_pdu_pP,
              pdcp_t            *pdcp_pP)
{
  protocol_ctxt_t   *dummy_ctxt_p;
  uint8_t           pdcp_header_len = 0;
  uint8_t           pdcp_tailer_len = 0;
  pdcp_sn_t         sequence_number = 0;
  lwa_entity_t      *lwa_p;
  boolean_t         is_next_pkt_rlc = 1; // 1: send to rlc; 0: send to WLAN
  rlc_op_status_t   rlc_status;
  wlan_op_status_t  wlan_status;

  // LOG_I(LWA, "[%s][test] ctxt_pP: module_id = %d, enb_flag = %d, instance = %d, rnti = %d, frame = %d, subframe = %d, eNB_index = %d\n",
  //       ctxt_pP->enb_flag? "eNB":" UE",
  //       ctxt_pP->module_id,
  //       ctxt_pP->enb_flag,
  //       ctxt_pP->instance,
  //       ctxt_pP->rnti,
  //       ctxt_pP->frame,
  //       ctxt_pP->subframe,
  //       ctxt_pP->eNB_index);
  
  //-------------------------------------------------------------------------------
  //LOG_D(PDCP, "[SHAUN] LWA is enabled\n");
  //lwa_util_print_hex_octets(LWA, (unsigned char*)pdcp_pdu_pP->data, 16);

  // Get the sequece number of the current pdcp pdu
  // Only for non-MBMS data
  if (MBMS_flagP == 0){
    if (srb_flagP){ //SRB1/2
      sequence_number = pdcp_get_sequence_number_of_pdu_with_SRB_sn((unsigned char*)pdcp_pdu_pP->data);
    }else{ //DRB
      if (pdcp_pP->seq_num_size == PDCP_SN_7BIT){
        sequence_number = pdcp_get_sequence_number_of_pdu_with_short_sn((unsigned char*) pdcp_pdu_pP->data);
      }else if (pdcp_pP->seq_num_size == PDCP_SN_12BIT){
        sequence_number = pdcp_get_sequence_number_of_pdu_with_long_sn((unsigned char*) pdcp_pdu_pP->data);
      }else{
        LOG_E(PDCP,
            PROTOCOL_PDCP_CTXT_FMT"wrong sequence number (%d) for this pdcp entity \n",
            PROTOCOL_PDCP_CTXT_ARGS(ctxt_pP, pdcp_pP),
            pdcp_pP->seq_num_size);
      }
    }
  }

  lwa_p = (struct lwa_entity_s *) pdcp_pP->lwa_entity;
  AssertFatal(lwa_p != NULL, "[SHAUN] LWA entity should have been initialized in pdcp_config_req_asn1()\n");

  lwa_p->current_seq_num = sequence_number;
  lwa_p->srb_flag = srb_flagP;

  /* Traffic control
      - decide which stream to send to: LTE or WLAN, based on sequence number, or other information
      - current scheme: pdu with old number goes to RLC; otherwise, WLAN, no matter whether it is a control- or user-plane packet
  */
  is_next_pkt_rlc = lwa_traffic_management(lwa_p);


  /* Traffic splitting */
  if (is_next_pkt_rlc == LWA_RLC){     // LTE
    LOG_N(LWA, "[SHAUN] %s sends a PDCP PDU (%p) data (%p) of size %d with SN %d (size %d) over LTE\n", 
          ctxt_pP->enb_flag? "eNB":" UE",
          pdcp_pdu_pP,
          pdcp_pdu_pP->data,
          pdcp_pdu_sizeP, 
          sequence_number, 
          pdcp_pP->seq_num_size);

    //lwa_util_print_hex_octets(LWA, (unsigned char*)pdcp_pdu_pP->data, 16);

    rlc_status = rlc_data_req(ctxt_pP, srb_flagP, MBMS_flagP, rb_idP, muiP, confirmP, pdcp_pdu_sizeP, pdcp_pdu_pP);

    return rlc_status;
  }else if (is_next_pkt_rlc == LWA_WLAN){                    // WLAN
    //LOG_I(LWA, "[SHAUN] sequence_number = %d, WLAN\n", sequence_number);
    LOG_N(LWA, "[SHAUN] %s sends a PDCP PDU (%p) data (%p) of size %d with SN %d (size %d) over WLAN\n", 
          ctxt_pP->enb_flag? "eNB":" UE",
          pdcp_pdu_pP,
          pdcp_pdu_pP->data,
          pdcp_pdu_sizeP, 
          sequence_number,
          pdcp_pP->seq_num_size);

    //lwa_util_print_hex_octets(LWA, (unsigned char*)pdcp_pdu_pP->data, 16);

    wlan_status = wlan_data_req(ctxt_pP, srb_flagP, MBMS_flagP, rb_idP, muiP, confirmP, pdcp_pdu_sizeP, pdcp_pdu_pP);

    /*--------------------------------------------------------------
                      LWA: we are cheating here
     ---------------------------------------------------------------*/
    dummy_ctxt_p = calloc(1, sizeof(struct protocol_ctxt_t*));
    dummy_ctxt_p->enb_flag = ctxt_pP->enb_flag? 0:1;
    dummy_ctxt_p->module_id = ctxt_pP->module_id;
    dummy_ctxt_p->rnti = ctxt_pP->rnti;

    // LOG_I(LWA, "[%s][test] ctxt_pP: module_id = %d, enb_flag = %d, instance = %d, rnti = %d, frame = %d, subframe = %d, eNB_index = %d\n",
    //     dummy_ctxt_p->enb_flag? "eNB":" UE",
    //     dummy_ctxt_p->module_id,
    //     dummy_ctxt_p->enb_flag,
    //     dummy_ctxt_p->instance,
    //     dummy_ctxt_p->rnti,
    //     dummy_ctxt_p->frame,
    //     dummy_ctxt_p->subframe,
    //     dummy_ctxt_p->eNB_index);

    wlan_data_ind(dummy_ctxt_p);
    free(dummy_ctxt_p);
    /*--------------------------------------------------------------*/

    return wlan_status;
  }else{
    LOG_E(LWA, "Error: neither RLC or WLAN\n");
    return WLAN_OP_STATUS_INTERNAL_ERROR;
  }
}


boolean_t 
lwa_data_ind( const protocol_ctxt_t* const  ctxt_pP,
              const srb_flag_t  srb_flagP,
              const MBMS_flag_t MBMS_flagP,
              const rb_id_t     rb_idP,
              const sdu_size_t  pdcp_pdu_sizeP,
              mem_block_t       *pdcp_pdu_pP,
              boolean_t         lte_flagP)
{
  pdcp_t          *pdcp_p           = NULL;
  lwa_entity_t    *lwa_p            = NULL;
  pdcp_sn_t       sequence_number = 0;

  /* Retrieve LWA context from PDCP since it is defined as a sublayer in PDCP */
  hash_key_t      key               = HASHTABLE_NOT_A_KEY_VALUE;
  hashtable_rc_t  h_rc;

  key = PDCP_COLL_KEY_VALUE(ctxt_pP->module_id, ctxt_pP->rnti, ctxt_pP->enb_flag, rb_idP, srb_flagP);
  h_rc = hashtable_get(pdcp_coll_p, key, (void**)&pdcp_p);

  if (h_rc != HASH_TABLE_OK) {
    LOG_W(PDCP,
          PROTOCOL_CTXT_FMT"Could not get PDCP instance key 0x%"PRIx64"\n",
          PROTOCOL_CTXT_ARGS(ctxt_pP),
          key);
    free_mem_block(pdcp_pdu_pP);
    return FALSE;
  }

  lwa_p = (struct lwa_entity_s *) pdcp_p->lwa_entity;
  AssertFatal(lwa_p != NULL, "[SHAUN] LWA entity should have been initialized in pdcp_config_req_asn1()\n");

  /* Retrieve PDCP PDU SN */
  if (MBMS_flagP == 0){
    if (srb_flagP){ //SRB1/2
      sequence_number = pdcp_get_sequence_number_of_pdu_with_SRB_sn((unsigned char*)pdcp_pdu_pP->data);
    }else{ //DRB
      if (pdcp_p->seq_num_size == PDCP_SN_7BIT){
        sequence_number = pdcp_get_sequence_number_of_pdu_with_short_sn((unsigned char*)pdcp_pdu_pP->data);
      }else if (pdcp_p->seq_num_size == PDCP_SN_12BIT){
        sequence_number = pdcp_get_sequence_number_of_pdu_with_long_sn((unsigned char*)pdcp_pdu_pP->data);
      }else{
        LOG_E(PDCP,
            PROTOCOL_PDCP_CTXT_FMT"wrong sequence number (%d) for this pdcp entity \n",
            PROTOCOL_PDCP_CTXT_ARGS(ctxt_pP, pdcp_p),
            pdcp_p->seq_num_size);
      }
    }
  }

  LOG_N(LWA, "[SHAUN] %s receives a PDCP PDU (%p) data (%p) of size %d with SN %d (size %d) over %s\n",
        ctxt_pP->enb_flag? "eNB":"UE",
        pdcp_pdu_pP,
        pdcp_pdu_pP->data,
        pdcp_pdu_sizeP,
        sequence_number,
        pdcp_p->seq_num_size,
        lte_flagP ? "LTE":"WLAN");
  
  //lwa_util_print_hex_octets(LWA, (unsigned char*)pdcp_pdu_pP->data, 16);

  /* Put it to LWA buffer */


  /* Find the packet with correct SN and pass it to PDCP layer */
  if (lwa_p->expected_rx_seq_num != sequence_number){
    LOG_W(LWA, "[SHAUN] %s expects %d, but receives %d\n",
          ctxt_pP->enb_flag? "eNB":"UE",
          lwa_p->expected_rx_seq_num,
          sequence_number);
  }else{
    // LOG_I(LWA, "[SHAUN] %s expects %d and receives %d, next one is %d\n",
    //       ctxt_pP->enb_flag? "eNB":"UE",
    //       lwa_p->expected_rx_seq_num,
    //       sequence_number,
    //       sequence_number + 1);
  }

  lwa_p->previous_rx_seq_num = sequence_number;
  lwa_p->expected_rx_seq_num = sequence_number + 1;

  //if (lte_flagP){
    pdcp_data_ind (
    ctxt_pP,
    srb_flagP,
    MBMS_flagP,
    rb_idP,
    pdcp_pdu_sizeP,
    pdcp_pdu_pP);
  //}
  
  display_mem_load ();
}

void lwa_entity_init(lwa_entity_t* lwa_pP){
  lwa_pP->is_defined = TRUE;
  lwa_pP->current_seq_num = 0;
  lwa_pP->previous_rx_seq_num = 0;
  lwa_pP->expected_rx_seq_num = 0;


  list_init(&lwa_pP->lwa_buffer, "LWA Buffer");
  lwa_pP->buffer_occupancy = 0;
}

void lwa_entity_free(lwa_entity_t *lwa_pP){
  if (&lwa_pP->lwa_buffer != NULL){
    list_free(&lwa_pP->lwa_buffer);
  }

  if (lwa_pP != NULL){
    free(lwa_pP);
  }
}


boolean_t lwa_traffic_management  (lwa_entity_t * const lwa_pP){
  if (lwa_pP->srb_flag){  // Control-plane
    return LWA_RLC;
  }else{                  // Data-plane
    if (lwa_pP->current_seq_num % 2){
      return LWA_RLC;     // Odd numbers go to RLC
    }else{
      return LWA_WLAN;    // Even numbers go to WLAN
    }
  }
}

void lwa_util_print_hex_octets(comp_name_t componentP, unsigned char* dataP, const signed long sizeP)
//-----------------------------------------------------------------------------
{
  unsigned long octet_index = 0;

  if (dataP == NULL) {
    return;
  }

  LOG_I(componentP, "+-----+-------------------------------------------------+\n");
  LOG_I(componentP, "|     |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |\n");
  LOG_I(componentP, "+-----+-------------------------------------------------+\n");

  for (octet_index = 0; octet_index < sizeP; octet_index++) {
    if ((octet_index % 16) == 0) {
      if (octet_index != 0) {
        //LOG_I(componentP, " |\n");
        printf(" |\n");
      }

      LOG_I(componentP, " %04d |", octet_index);
    }

    /*
     * Print every single octet in hexadecimal form
     */
    //LOG_I(componentP, " %02x", dataP[octet_index]);
    printf(" %2x", dataP[octet_index]);
    /*
     * Align newline and pipes according to the octets in groups of 2
     */
  }

  /*
   * Append enough spaces and put final pipe
   */
  unsigned char index;

  for (index = octet_index; index < 16; ++index) {
    //LOG_I(componentP, "   ");
    printf("   ");
  }

  //LOG_I(componentP, " |\n");
  printf(" |\n");
}