# Simulation: eNB (without S1) <-> UE

## Build
```
cd ~/openair4G_LWA/cmake_targets
sudo ./build_oai -g -x --oaisim --noS1
```

## Run Simulation
In `~/openair4G_LWA/cmake_targets/tools`, run

* `./run_enb_ue_virt_noS1 -c ../../targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_no_mme.conf -K /tmp/itti.log -m /tmp -W -g`
* `./run_enb_ue_virt_noS1 -c ../../targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_no_mme.conf -K /tmp/itti.log`

Or in `~/openair4G_LWA/simu_LWA`, run `./run_simu`

## Help message:
```
Usage: start_enb_ue_virt_noS1 [OPTION]...
Run the eNB and/or UE executable with no hardware.
 
Options:
Mandatory arguments to long options are mandatory for short options too.
  -c, -C, --config-file  eNB_config_file  eNB config file, (see /home/labuser/openair4G_LWA/targets/PROJECTS/GENERIC-LTE-EPC/CONF)
                                          Default eNB config file if not set is /home/labuser/openair4G_LWA/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.generic.oaisim.local_no_mme.conf
  -g, --gdb                               Run with GDB.
  -h, --help                              Print this help.
  -K, --itti-dump-file   filename         ITTI dump file containing all ITTI events occuring during EPC runtime.(can omit file name if last argument)
  -m, --mscgen           directory        Generate mscgen output files in a directory
  -V, --vcd                               Dump timings of processing in a GTKWave compliant file format.
  -W, --wireshark-l2                      Dump MAC frames for visualization with wireshark. You need to open Wireshark, open the preferences, and check try heuristics for the UDP protocol, MAC-LTE, RLC-LTE,
                                          and PDCP-LTE. Then capture for all the interfaces with the following filters: s1ap or lte_rrc or mac-lte or rlc-lte or pdcp-lte. Note the L2 pdus are transmitted to the local interface.
  -x, --xforms                            Run XFORMS scope windows.
```

**_Notes_**:

1. All log files are in /tmp
2. To use ITTI analyzer to exam itti.log, the procedure is as follows:
  ```
  sudo apt-get install libgtk-3-0
  cd ~/openair4G_LWA/common/utils/itti_analyzer
  autoreconf -fi && ./configure && make && sudo make install
  ./itti_analyzer -m /tmp/itti.log
  ```
3. How to ping each other
    * Traffic from eNB0-UE0: ping 10.0.1.2
    * Traffic from UE0-eNB0: ping 10.0.2.1

## PDCP Layer in OAI Simulation

1. In `oaisim.c/main()`, `create_tasks()` is called, which creates all threads including `oaisim.c/l2l1_tasks()` (running as a routine);
2. In `oaisim.c/l2l1_tasks()`, `pdcp.c/pdcp_run()` is called, which reads SDUs from either OTG buffer or NETLINK buffer;
3. `pdcp_run()` calls `pdcp_fifo_read_input_sdus_from_otg()` and `pdcp_fifo_read_input_sdus` in `pdcp_fifo.c`, where `pdcp.c/pdcp_data_req()` is called.