Branches:

1. master: 
	* my own working copy; 
	* may pull from latest_trunk
2. latest_trunk: 
	* sync with http://svn.eurecom.fr/openair4G/trunk
3. DL_throughput: 
	* Nokia EPC <-> OAI eNB <-> COTS UE; 
	* may pull from latest_trunk
4. r7763:
	* OAI eNB <-> OAI UE works
5. LWA:
	* LTE-WiFi aggregation
